#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<time.h>

//structure that defines bitmap header
struct BITMAPFILEHEADER {
	uint8_t type[2]; //type of file (bit map)
	uint32_t size; //size of file
	uint16_t reserved1; //
	uint16_t reserved2; //
	uint32_t offsetbits; //off set bits
}__attribute__((packed));

struct BITMAPINFOHEADER {
	uint32_t size; //bitmap size
	//uint16_t w2;
	uint32_t width;   //width of bitmap
	//uint16_t h2;
	uint32_t height;   //hight of bitmap

	uint16_t planes;
	uint16_t bitcount;
	uint32_t compression;   // compression ratio (zero for no compression)
	uint32_t sizeimage;   //size of image
	long xpelspermeter;
	long ypelspermeter;
	uint32_t colorsused;
	uint32_t colorsimportant;
}__attribute__((packed));


int main(int argc,const char* argv[]) {
    int i,j;

    struct BITMAPFILEHEADER source_header;
    struct BITMAPINFOHEADER source_info;

    unsigned char **in = NULL;
    unsigned char **out = NULL;

    FILE *fin = fopen("rose3.bmp","rb");
    FILE *fout = fopen("output.bmp","wb");

    if(fin == NULL){
        printf("The input file couldn't be opened.");
        exit(1);
    }
    if(fout == NULL){
        printf("The output file couldn't be opened.");
        exit(1);
    }

    // reading and saving pixels in a matrix from the rose3.bmp file
    fread(&source_header,sizeof(struct BITMAPFILEHEADER),1,fin);
    fread(&source_info,sizeof(struct BITMAPINFOHEADER),1,fin);

    in = (unsigned char **) malloc(sizeof(unsigned char*)* source_info.height);

    for(i = 0; i < source_info.height;i++){
        in[i] = (unsigned char *) malloc(sizeof(unsigned char)* source_info.width);
    }

    out = (unsigned char **) malloc(sizeof(unsigned char*)* source_info.height);
    for(i = 0; i < source_info.height;i++){
        out[i] = (unsigned char *) malloc(sizeof(unsigned char)* source_info.width);
    }

    for(i = 0;i < source_info.height;i++){
        for(j = 0; j < source_info.width;j++){
            fread(&in[i][j],sizeof(unsigned char),1,fin);
        }
    }

    fwrite(&source_header,sizeof(struct BITMAPFILEHEADER),1,fout);
    fwrite(&source_info,sizeof(struct BITMAPINFOHEADER),1,fout);


    for(i = 0;i < source_info.height;i++){
        for(j = 0; j < source_info.width;j++){
            fwrite(&in[i][j],sizeof(unsigned char),1,fout);
        }
    }
    fclose(fin);
    fclose(fout);

    return 0;
}
